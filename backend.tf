terraform {
  backend "s3" {
    bucket         = "terraform-state-vinay"
    key            = "vinay-terraform-state"
    region         = "us-east-1"
    dynamodb_table = "terraform_locks"  # Should match the actual name of your DynamoDB table
  }
}

resource "aws_dynamodb_table" "terraform_locks" {
  name           = "terraform-state-locking"
  billing_mode   = "PAY_PER_REQUEST"
  hash_key       = "LockID"  # Corrected from has_key to hash_key
  attribute {
    name = "LockID"
    type = "S"
  }
}
